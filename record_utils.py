#!/usr/bin/env python3

# imports
import csv
import os
from typing import List


def create_directory(path: str = './temperature_measurements.csv'):
    """
    Creates directory to path if not present.

    :param path: path including file
    """
    file_name = os.path.dirname(path)
    if not os.path.exists(file_name):
        os.makedirs(file_name, exist_ok=True)


def create_file(path: str = './temperature_measurements.csv',
                fields: List[str] = None):
    """
    Creates a new csv file for temperature measurements.

    :param fields: fields for the new file to be created
    :param path: path to save the file to incl. file name
    """
    if fields is None:
        fields = ['timestamp', 'temperature', 'humidity']

    # creates path directories if not already present
    create_directory(path=path)

    with open(path, 'w') as file:
        csv_writer = csv.writer(file, delimiter=',')
        csv_writer.writerow(fields)


def append_measurement(measurements: List[float],
                       path: str = './temperature_measurements.csv',
                       fields: List[str] = None):
    """
    Writes new measurements to file.

    :param measurements: measurements in the same order as fields
    :param path: path to file
    :param fields: fields indicating the measurements
    """
    if not os.path.isfile(path=path):
        create_file(path=path, fields=fields)

    with open(path, 'a') as file:
        csv_writer = csv.writer(file, delimiter=',')
        csv_writer.writerow(measurements)
