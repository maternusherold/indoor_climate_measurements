#!/usr/bin/env python3


import Adafruit_DHT
import time

from climate_sensor import DHT22Sensor
from record_utils import append_measurement


def measure_climate(sampling_rate: int = 60,
                    file: str = './temperature_measurements.csv'):
    """
    Takes a climate measurement from DHT22 sensor and save to file.

    :param sampling_rate: time in seconds between measurements
    :param file: file to save the cantered measurement to
    :return: List of timestamp, temperature and humidity measurement
    """
    # infinitely capture climate measurements
    while True:
        # list of timestamp, temperature, humidity
        measurement = sensor.take_measurement()
        append_measurement(measurements=measurement, path=file,
                           fields=['timestamp', 'temperature', 'humidity'])
        # take measurement every 60 seconds
        time.sleep(sampling_rate)


if __name__ == '__main__':

    # file parameters
    file_path = './recorded_data/climate_stats.csv'

    # set constants for connections on the Pi
    DHT_SENSOR_ = Adafruit_DHT.DHT22  # sensor type
    DHT_PIN_ = 4  # GPIO pin the sensor is connected to
    sensor = DHT22Sensor(sensor_config=DHT_SENSOR_, gpio_pin_number=DHT_PIN_)

    # constantly take measurement
    measure_climate(sampling_rate=60, file=file_path)
