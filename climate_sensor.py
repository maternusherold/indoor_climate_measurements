#!/usr/bin/env python3

# imports
import Adafruit_DHT
import datetime
from typing import List


class DHT22Sensor:

    def __init__(self, sensor_config: Adafruit_DHT.DHT22, gpio_pin_number: int):
        """
        Instance of the configured and connected DHT22 sensor.

        :param sensor_config: sensor configuration
        :param gpio_pin_number: GPIO pin the sensor connected to
        """
        self.sensor = sensor_config
        self.gpio_pin = gpio_pin_number

    def take_measurement(self) -> List[float]:
        """
        Takes a climate measurement of temperature and humidity.
        :return: List of timestamp and measurements
        """
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.gpio_pin)
        if self.stats_ok(temperature, humidity):
            timestamp = datetime.datetime.timestamp(datetime.datetime.now())
            return [timestamp, temperature, humidity]

    @staticmethod
    def stats_ok(temperature, humidity) -> bool:
        """
        Checks if the measurements are valid floats.

        :param temperature: temperature measurement
        :param humidity: humidity measurement
        :return: boolean indicating the measurement validity
        """
        status = False
        if humidity is not None and temperature is not None:
            status = True
        else:
            raise ValueError(f'sensor returned invalid measurement - '
                             f'temperature: {temperature}, humidity: {humidity}')
        return status
